import threading
import queue
import time


class User(threading.Thread):
    def __init__(self, chat_bot_name, user_to_bot_q, bot_to_user_q):
        threading.Thread.__init__(self)
        self.user_to_bot_q = user_to_bot_q
        self.bot_to_user_q = bot_to_user_q
        self.chat_bot_name = chat_bot_name
        self.user_input = []

    def run(self):
        while True:
            if self.user_input:
                self.user_to_bot_q.put(self.user_input[0])
                self.user_input.pop(0)
            try:
                chat_bot_input = self.bot_to_user_q.get(False)
                self.typing_animation(len(chat_bot_input))
                print("{}: {}".format(self.chat_bot_name, chat_bot_input))
            except queue.Empty:
                pass

    def typing_animation(self, txt_length: int):
        base_string = "{} is typing".format(self.chat_bot_name)
        dots = [base_string, base_string + ".", base_string + "..", base_string + "..."]
        time_start = time.time_ns()
        time_stop = 0
        while (time_stop - time_start)/1e9 <= 0.05*txt_length:
            for dot in dots:
                time.sleep(0.3)
                print("\r" + " "*(len(self.chat_bot_name)+13), end="")
                print("\r" + dot, end="")
                time_stop = time.time_ns()
        print("\r" + " "*(len(self.chat_bot_name)+13) + "\r", end="")



