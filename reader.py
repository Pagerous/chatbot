import logging
from nltk import word_tokenize, pos_tag, sent_tokenize, RegexpParser
from chatbot_knowledge import words_filter

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.WARNING)


class Reader:
    def __init__(self, text_to_read: str, max_space: int):
        self.user_question = ""
        self.text = text_to_read
        self.max_space = max_space
        self.nouns = []
        self.other_words = []
        self.noun_lack_lvl = 0.5
        self.others_lack_lvl = 0.5

    def special_words_extraction(self):
        self.nouns.clear()
        NP = r"NP: {<NN\w*>+}"
        cp = RegexpParser(NP)
        result = cp.parse(pos_tag(word_tokenize(self.user_question)))
        for subtree in result.subtrees(filter=lambda x: x.label() in ['NP']):
            self.nouns.append(" ".join(element[0] for element in subtree).rstrip())
        self.other_words = words_filter(self.nouns, self.user_question)

    def extract_potential_answer(self, message: str):
        self.user_question = message
        self.special_words_extraction()
        sentences = sent_tokenize(self.text)
        if self.nouns:
            return self.searching_with_nouns(sentences)
        elif self.other_words:
            return self.search_without_nouns(sentences)
        else:
            return "I don't know."

    def searching_with_nouns(self, sentences):
        information = []
        occ_nouns = {noun: False for noun in self.nouns}
        occ_sents = {noun: 0 for noun in self.nouns}
        best_fits = []
        logging.info("nouns: {}".format(self.nouns))
        logging.info("others: {}".format(self.other_words))
        for i, sent in enumerate(sentences):
            # Sprawdzanie jakie rzeczowniki wystepuja w linijce
            for noun in self.nouns:
                if noun in sent:
                    logging.info("found noun: {}".format(noun))
                    if not occ_nouns[noun]:
                        logging.info("first found noun: {}, {}".format(noun, i))
                        occ_nouns[noun] = True
                        occ_sents[noun] = i
                if i - occ_sents[noun] > self.max_space:
                    occ_nouns[noun] = False
            # Sprawdzanie czy wystapila juz wystarczajaca liczba rzeczownikow:
            nouns_occured = [occ_sents[noun] for noun in occ_sents if occ_nouns[noun]]
            nouns_num = len(nouns_occured)
            ratio = nouns_num / len(self.nouns)
            if ratio > self.noun_lack_lvl:
                if nouns_occured not in best_fits:
                    best_fits.append(nouns_occured)

        try:
            best_fit_len = max(len(best_fit) for best_fit in best_fits)
        except ValueError:
            best_fit_len = len(self.nouns) + 1

        logging.info("best_fit_len: {}".format(best_fit_len))
        for best in [best_fit for best_fit in best_fits if len(best_fit) == best_fit_len]:
            logging.info("best: {}".format(best))
            earlier_sent, latest_sent = min(best), max(best)
            entry = " ".join(sentences[earlier_sent:latest_sent + 1])
            logging.info("entry: {}".format(entry))
            if entry not in information:
                others_num = 0
                reg = []
                if self.other_words:
                    for other in self.other_words:
                        if other in entry:
                            others_num += 1
                            reg.append(other)
                    if others_num / len(self.other_words) > self.others_lack_lvl:
                        information.append(entry)
                else:
                    information.append(entry)
        if information:
            return " ".join(information)
        else:
            return "I don't know."

    def search_without_nouns(self, sentences):
        information = []
        occ_words = {word: False for word in self.other_words}
        occ_sents = {word: 0 for word in self.other_words}
        best_fits = []
        logging.info("others: {}".format(self.other_words))
        for i, sent in enumerate(sentences):
            # Sprawdzanie jakie slowa wystepuja w linijce
            for word in self.other_words:
                if word in sent:
                    logging.info("found word: {}".format(word))
                    if not occ_words[word]:
                        logging.info("first found word: {}, {}".format(word, i))
                        occ_words[word] = True
                        occ_sents[word] = i
                if i - occ_sents[word] > self.max_space:
                    occ_words[word] = False
            # Sprawdzanie czy wystapila juz wystarczajaca liczba rzeczownikow:
            words_occured = [occ_sents[word] for word in occ_sents if occ_words[word]]
            words_num = len(words_occured)
            ratio = words_num / len(self.other_words)
            if ratio > self.others_lack_lvl:
                if words_occured not in best_fits:
                    best_fits.append(words_occured)

        try:
            best_fit_len = max(len(best_fit) for best_fit in best_fits)
        except ValueError:
            best_fit_len = len(self.other_words) + 1

        logging.info("best_fit_len: {}".format(best_fit_len))
        for best in [best_fit for best_fit in best_fits if len(best_fit) == best_fit_len]:
            logging.info("best: {}".format(best))
            earlier_sent, latest_sent = min(best), max(best)
            entry = " ".join(sentences[earlier_sent:latest_sent + 1])
            logging.info("entry: {}".format(entry))
            if entry not in information:
                information.append(entry)
        if information:
            return " ".join(information)
        else:
            return "I don't know."

