import os
import logging
import json
import copy
from nltk import word_tokenize, sent_tokenize, pos_tag, RegexpParser
from nltk.corpus import stopwords, wordnet

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.WARNING)

class KnowledgeRepository:
    def __init__(self, repository_path: str):
        self.repository_path = repository_path
        self.create_file()
        self.information = {}
        self.nouns_lack_lvl = 0.25
        self.others_lack_lvl = 0.25

    def create_file(self):
        try:
            with open(self.repository_path, 'r') as f:
                pass
        except FileNotFoundError:
            with open(self.repository_path, 'w') as f:
                pass

    def create_account(self, user_name: str):
        try:
            os.mkdir("user_information/{}".format(user_name.lower()))
        except FileExistsError:
            pass
        open("user_information/{}/{}-links.txt".format(user_name.lower(), user_name.lower()), "a").close()

    def information_extraction(self, message: str):
        NP = r"NP: {<NN\w*>+}"
        cp = RegexpParser(NP)
        result = cp.parse(pos_tag(word_tokenize(message)))
        entities = []
        for subtree in result.subtrees(filter=lambda x: x.label() in ['NP']):
            entities.append(" ".join(element[0] for element in subtree).rstrip())
        self.save_info(entities, message)

    def save_info(self, entities: list, information: str) -> None:
        with open(self.repository_path, 'r+') as json_file:
            try:
                data = json.load(json_file)
            except json.JSONDecodeError:
                data = {}
            for major_entity in entities:
                logging.info("major entity: {}".format(major_entity))
                if major_entity not in data:
                    logging.info("major entity: {}, creating record...".format(major_entity))
                    data[major_entity] = {}
                if 'whole_story' not in data[major_entity]:
                    data[major_entity]['whole_story'] = []
                if information not in data[major_entity]['whole_story']:
                    data[major_entity]['whole_story'].append(information)
                for entity in entities:
                    if entity != major_entity:
                        logging.info("entity: {}".format(entity))
                        if entity not in data[major_entity]:
                            logging.info("entity: {}, creating record...".format(entity))
                            data[major_entity][entity] = []
                        if information not in data[major_entity][entity]:
                            logging.info("entity: {}, updating record...".format(entity))
                            data[major_entity][entity].append(information)
            json_file.seek(0)
            json.dump(data, json_file)
            json_file.truncate()

    def get_information(self, message: str) -> str:
        noun_phrase_re = r"NP: {<NN\w*>+}"
        cp = RegexpParser(noun_phrase_re)
        noun_phrase = cp.parse(pos_tag(word_tokenize(message)))
        entities = []
        for subtree in noun_phrase.subtrees(filter=lambda x: x.label() in ['NP']):
            entities.append(" ".join(element[0] for element in subtree).rstrip())
        information = self.get_info(entities, message)
        if information:
            return " ".join(information)
        else:
            return "I don't know."

    def get_info(self, noun_entities,  message):
        try:
            with open(self.repository_path, 'r') as json_file:
                data = json.load(json_file)
                logging.info("all noun entities: {}".format(noun_entities))

                # Zbieranie znaczacych wyrazow, innych niz rzeczowniki
                others = words_filter(noun_entities, message)
                logging.info("other entities: {}".format(others))
                if noun_entities:
                    return self.searching_with_nouns(noun_entities, others, data)
                elif others:
                    return self.searching_without_nouns(others, data)
                else:
                    return []
        except FileNotFoundError:
            with open(self.repository_path, 'w') as f:
                pass
        except json.decoder.JSONDecodeError:
            return []

    def searching_with_nouns(self, noun_entities, others, data):
        information_package = []
        # Sprawdzanie powiazan miedzy rzeczownikami i innymi waznymi slowami
        accurate = [(ent, look_connections(ent, noun_entities, others, data)) for ent in noun_entities]
        logging.info("accurate nouns: {}".format(accurate))
        for major_noun, (nouns_num, connections) in accurate:
            if nouns_num / len(noun_entities) >= self.nouns_lack_lvl:
                if others:
                    for sub_noun, (words_num, reg) in connections:
                        if words_num / len(others) >= self.others_lack_lvl:
                            smallest_cut = get_smallest_cut(reg, data[major_noun][sub_noun])
                            logging.info("fulfilling lack condition: {}, {}, {}".format(major_noun, sub_noun, reg))
                            if smallest_cut not in information_package:
                                information_package.append(smallest_cut)
                else:
                    information_package.append(" ".join(data[major_noun]['whole_story']))
        return information_package

    def searching_without_nouns(self, others, data):
        information_package = []
        for major_noun in data:
            for sub_noun in data[major_noun]:
                whole_entry = " ".join(data[major_noun][sub_noun])
                counter = 0
                reg = []
                for other in others:
                    if other in whole_entry:
                        counter += 1
                        reg.append(other)
                if counter/len(others) > self.others_lack_lvl:
                    smallest_cut = get_smallest_cut(reg, data[major_noun][sub_noun])
                    if smallest_cut not in information_package:
                        information_package.append(smallest_cut)
        return information_package


def look_connections(examined_entity, entities, other_words, data) -> (int, list):
    nouns_num = 0
    connections = []
    examined_entities = copy.copy(entities)
    examined_entities.remove(examined_entity)
    found = False
    if examined_entity in data:
        for entity in examined_entities:
            if entity in data[examined_entity]:
                nouns_num += 1
                connections.append((entity, check_others(other_words, data[examined_entity][entity])))
                found = True

            # # TUTAJ JEST PROBLEM
            # else:
            #     for synonym in get_all_synonyms(entity):
            #         if synonym in data[examined_entity]:
            #             nouns_num += 1
            #             connections.append((synonym, self.check_others(examined_entity, synonym, other_words, data)))
            #         break
        if not found or len(examined_entities) == 0:
            nouns_num += 1
            connections.append(('whole_story', check_others(other_words, data[examined_entity]['whole_story'])))
    return nouns_num, connections


def check_others(others: list, data) -> (int, list):
    words_num = 0
    reg = []
    whole_entry = " ".join(data)
    for word in others:
        if word in whole_entry:
            words_num += 1
            reg.append(word)

        # # TUTAJ JEST PROBLEM
        # else:
        #     for synonym in get_all_synonyms(word):
        #         if synonym in whole_entry:
        #             words_num += 1
        #             reg.append(synonym)
    return words_num, reg


def get_smallest_cut(obligatory_words, data):
    first_index, last_index = None, None
    got_first = False
    for i, entry in enumerate(data):
        for word in obligatory_words:
            if word in entry:
                if not got_first:
                    first_index = i
                    got_first = True
                last_index = i
    return " ".join(data[first_index:last_index+1])


def get_all_synonyms(to_what: str):
    synonyms_set = []
    whole_synset = wordnet.synsets(to_what) # slow performance!
    for synset in whole_synset:
        synonyms_set.extend(synset.lemma_names())
    return list(set(synonyms_set))


def words_filter(to_exclude: list, message: str) -> list:
    words = []
    for word in word_tokenize(message):
        if word.lower() not in stopwords.words('english') + [ex.lower() for ex in to_exclude] + ["?", "!", ".", ","]:
            words.append(word)
    return words



