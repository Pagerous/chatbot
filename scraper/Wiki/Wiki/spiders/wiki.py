# -*- coding: utf-8 -*-
import scrapy
from ..items import WikiItem


class WikiSpider(scrapy.Spider):
    name = 'wiki'
    start_urls = ['https://en.wikipedia.org/wiki/Chatbot']
    content = []

    def parse(self, response):
        items = WikiItem()
        paragraphs = response.css("p")
        for paragraph in paragraphs:
            text = paragraph.css('::text').extract()
            text = self.concatenate(text)
            items['paragraph'] = text
            self.content.append(text)
            yield items

    def concatenate(self, text):
        textout = ''
        for el in text:
            textout += el
        return textout

