from scrapy.crawler import CrawlerProcess
from scraper.Wiki.Wiki.spiders.wiki import WikiSpider


def wiki_scraper(link: str):

    print("LINK: <{}>".format(link))
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })
    WikiSpider.name = 'wiki'
    WikiSpider.start_urls = [link]
    process.crawl(WikiSpider)
    process.start(stop_after_crawl=True)
    content = WikiSpider.content
    return " ".join(content)


if __name__ == "__main__":
    content = wiki_scraper("https://en.wikipedia.org/wiki/Krak%C3%B3w")
    print(content)