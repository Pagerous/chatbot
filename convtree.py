

class ConversationTree:
    def __init__(self):
        self.tree = {}
        self.current_subtree = {}
        self.active_question = None
        self.question_list = []
        self.received_answer = {}

    def create_conversation(self, conversation_tree: dict):
        self.tree = conversation_tree
        self.current_subtree = self.tree
        self.active_question = list(self.tree.keys())[0]
        self.question_list.append(self.active_question)

    def answer(self, message: str):
        found, (key, entity) = self.active_question.process_answer(message)
        self.received_answer[key] = entity
        if found:
            try:
                self.active_question = list(self.retrieve_subtree(self.tree, correct=True).keys())[0]
            except AttributeError:
                self.active_question = self.retrieve_subtree(self.tree, correct=True)
            if self.active_question == "DONE":
                return True, self.received_answer
            self.active_question.memory = entity
            self.add_question_to_hierarchy(self.active_question)
        else:
            self.x_nodes_back(self.retrieve_subtree(self.tree)['incorr_entity'])
        return False, self.active_question.question.replace("{}", self.active_question.memory)

    def x_nodes_back(self, x: int):
        tmp_index = self.question_list.index(self.active_question)-x
        self.active_question = self.question_list[self.question_list.index(self.active_question)-x]
        self.question_list = self.question_list[:tmp_index+1]

    def add_question_to_hierarchy(self, question):
        if question not in self.question_list:
            self.question_list.append(question)

    def retrieve_subtree(self, tree: dict, correct=False):
        if tree:
            for node in tree:
                if node == self.active_question:
                    if correct:
                        return tree[self.active_question]['corr_entity']
                    else:
                        return tree[self.active_question]
                else:
                    return self.retrieve_subtree(tree[node], correct)

    def restart(self):
        self.active_question = list(self.tree.keys())[0]
        self.question_list.clear()
        self.question_list.append(self.active_question)
