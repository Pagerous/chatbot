from nltk import pos_tag, word_tokenize, RegexpParser
from nltk.corpus import wordnet as wn

def information_extraction(message: str):
    NP = r"NP: {<NN\w*>+}"
    cp = RegexpParser(NP)
    result = cp.parse(pos_tag(word_tokenize(message)))
    for subtree in result.subtrees(filter=lambda x: x.label() in ['NP']):
        print(" ".join(element[0] for element in subtree).lstrip())
# information_extraction("Arkadiusz Pajor is studying.")


def get_all_synonyms(to_what):
    print(to_what)
    synonyms_set = []
    print("jestem w syn")

    for synset in wn.synsets(to_what):
        print("synset")
        print(type(synset.lemma_names()))
        synonyms_set.extend(synset.lemma_names())
    print("after 2")
    return list(set(synonyms_set))

print(get_all_synonyms('largest'))
