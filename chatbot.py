import threading
import queue
import chatbot_knowledge as ch_k
from question import Question
from convtree import ConversationTree
from reader import Reader


class ChatBot(threading.Thread):
    def __init__(self, chat_bot_name, user_to_bot_q, bot_to_user_q):
        threading.Thread.__init__(self)
        self.name = chat_bot_name
        self.user_name = None
        self.bot_to_user_q = bot_to_user_q
        self.user_to_bot_q = user_to_bot_q
        self.chat_bot_inputs = []
        self.repository = ch_k.KnowledgeRepository("knowledge_repository/"
                                                   "{}.json".format(self.name.replace(" ", "").lower()))
        self.open_conv = ConversationTree()
        self.open_conv.create_conversation({Question("Hi my friend, what is your name?",
                                                     pos_regexp_pattern=r"NP: {<NNP\w*>+}",
                                                     phrases=['NP'], key='name'): {
            'corr_entity': {Question("Is this your name: '{}'?",
                                     expected_answers=['yes', 'no'],
                                     key='yesno',
                                     ignore_case=True): {
                'corr_entity': "DONE",
                'incorr_entity': 0}},
            'incorr_entity': 0
            }})
        self.temp_conv = ConversationTree()
        self.ready_to_save = ""
        self.reader = None
        self.opening = True
        self.freeze = False
        self.open_mind = False
        self.questions_to_huge_text = False
        self.block = False

    def run(self):
        while True:
            try:
                user_input = self.user_to_bot_q.get(False)
            except queue.Empty:
                if self.chat_bot_inputs:
                    self.bot_to_user_q.put(self.chat_bot_inputs[0])
                    self.chat_bot_inputs.pop(0)
            else:
                self.process_message(user_input)

    def process_message(self, message: str):
        if self.opening:
            self.opening_procedure(message)
        elif not self.freeze and not self.questions_to_huge_text:
            if "freeze all motor functions" in message.lower():
                self.chat_bot_inputs.append("The motor functions have been stopped. "
                                            "I will undoubtedly accept any information you provide.")
                self.freeze = True
            elif "want to ask" in message.lower():
                self.chat_bot_inputs.append("Nice! What do you want to know?")
                self.questions_to_huge_text = True
            else:
                self.chat_bot_inputs.append(self.repository.get_information(message))
        elif self.freeze:
            if "run all motor functions" in message.lower():
                self.freeze = False
                self.chat_bot_inputs.append("I am ready. Ask me anything.")
            elif "open your mind" in message.lower() and not self.open_mind:
                self.open_mind = True
                self.chat_bot_inputs.append("I am listening...")
            elif self.open_mind:
                self.reader = Reader(message, 1)
                self.chat_bot_inputs.append("It's interesting!")
                self.open_mind = False
            else:
                self.repository.information_extraction(message)
        elif self.questions_to_huge_text:
            if "go back now" in message.lower():
                self.block = False
                self.questions_to_huge_text = False
                self.ready_to_save = ""
                self.chat_bot_inputs.append("Whatever you want.")

            elif not self.block:
                chat_bot_answer = self.reader.extract_potential_answer(message)
                self.ready_to_save = chat_bot_answer
                self.chat_bot_inputs.append(chat_bot_answer)
                if chat_bot_answer != "I don't know.":
                    self.temp_conv.create_conversation({Question("Is this answer satifying you?",
                                                                 expected_answers=['yes', 'no'],
                                                                 key='yesno',
                                                                 ignore_case=True):
                                                          {'corr_entity': 'DONE',
                                                           'incorr_entity': 0}})
                    self.chat_bot_inputs.append(self.temp_conv.active_question.question)
                    self.block = True
            else:
                finished, chat_bot_message = self.temp_conv.answer(message)
                if finished:
                    if chat_bot_message['yesno'] == 'no':
                        self.chat_bot_inputs.append("Sorry for disappointing you :(")
                        self.ready_to_save = ""
                    else:
                        self.chat_bot_inputs.append("Ok, I will remember that.")
                        self.repository.information_extraction(self.ready_to_save)
                    self.block = False
                else:
                    self.chat_bot_inputs.append(chat_bot_message)


    def opening_procedure(self, message):
        finished, chat_bot_message = self.open_conv.answer(message)
        if finished:
            if chat_bot_message['yesno'] == "no":
                self.chat_bot_inputs.append("Am I joke to you?")
                self.open_conv.restart()
            else:
                self.opening = False
                self.chat_bot_inputs.append("Thank you, {}.".format(chat_bot_message['name']))
                self.repository.create_account(chat_bot_message['name'])
        else:
            self.chat_bot_inputs.append(chat_bot_message)


