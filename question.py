from nltk import word_tokenize, pos_tag, RegexpParser


class Question:
    def __init__(self, question, expected_answers="", pos_regexp_pattern="", phrases="", key="", ignore_case=False):
        self.question = question
        self.expected_answers = expected_answers
        self.pos_regexp_pattern = pos_regexp_pattern
        self.phrases = phrases
        self.ignore_case = ignore_case
        self.key = key
        self.user_answer = ""
        self.memory = ""

    def process_answer(self, message):
        self.user_answer = message
        if self.ignore_case:
            self.user_answer = self.user_answer.lower()
        entities = []
        if self.pos_regexp_pattern and self.phrases:
            cp = RegexpParser(self.pos_regexp_pattern)
            result = cp.parse(pos_tag(word_tokenize(message)))
            for subtree in result.subtrees(filter=lambda x: x.label() in self.phrases):
                entities.append(" ".join(element[0] for element in subtree).rstrip())
        if self.expected_answers:
            for answer in self.expected_answers:
                if answer in self.user_answer:
                    entities.append(answer)
        if entities:
            return True, (self.key, entities[0])
        else:
            return False, (self.key, "")
