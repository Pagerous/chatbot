import queue
import chatbot
import user
import os


class ChatRoom:
    def __init__(self, chat_bot_name):
        self.user_to_bot_q = queue.Queue()
        self.bot_to_user_q = queue.Queue()
        try:
            os.mkdir("knowledge_repository")
            os.mkdir("user_information")
        except FileExistsError:
            pass
        self.chat_bot = chatbot.ChatBot(chat_bot_name, self.user_to_bot_q, self.bot_to_user_q)
        self.user = user.User(chat_bot_name, self.user_to_bot_q, self.bot_to_user_q)

    def chat_with_bot(self):
        self.chat_bot.start()
        self.user.start()
        while True:
            self.user.user_input.append(input())


chat_room = ChatRoom("Grey Worm")
chat_room.chat_with_bot()
